import { browser, by, element } from 'protractor';

export class AppPage {
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl);
  }

  async doesInputExist(id: string): Promise<boolean> {
    return element(by.css('input#' + id)).isPresent();
  }

  async getInputValue(id: string): Promise<string> {
    return element(by.css('input#' + id)).getAttribute("value");
  }

  async doesButtonExist(id: string): Promise<boolean> {
    return element(by.css('button#' + id)).isPresent();
  }

  async enterInputText(id: string, text: string): Promise<void> {
    element(by.css('input#' + id)).clear();
    return element(by.css('input#' + id)).sendKeys(text);
  }

  async clickButton(id: string): Promise<void> {
    return element(by.css('button#' + id)).click();
  }
/*
  async isButtonEnabled(id: string): Promise<boolean> {
    return element(by.css('button#' + id)).isEnabled();
  }
 */

  async enterCustomerInfo(firstname: string, lastName: string, address: string, phoneNumber: string): Promise<void> {
    await this.enterInputText('firstName', firstname);
    await this.enterInputText('lastName', lastName);
    await this.enterInputText('address', address);
    await this.enterInputText('phoneNumber', phoneNumber);
  }

  async findInList(firstName: string, lastName: string): Promise<boolean> {
    return element(by.cssContainingText('ul#customerList li', firstName + ' ' + lastName)).isPresent();
  }

  async clickListItem(index: number): Promise<void> {
    return element.all(by.css('ul#customerList li')).get(index).click();
  }

  async findText(id: string, query: string): Promise<boolean> {
    return element(by.cssContainingText('#' + id, query)).isPresent();
  }

  async editCustomerInfo(firstname: string, lastName: string, address: string, phoneNumber: string): Promise<void> {
    await this.enterInputText('firstNameEdit', firstname);
    await this.enterInputText('lastNameEdit', lastName);
    await this.enterInputText('addressEdit', address);
    await this.enterInputText('phoneNumberEdit', phoneNumber);
  }
}
