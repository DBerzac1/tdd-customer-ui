import { browser, logging } from 'protractor';
import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('form fields and button work', async () => {
    await page.navigateTo();
    expect(await page.doesInputExist('firstName')).toBe(true);
    expect(await page.doesInputExist('lastName')).toBe(true);
    expect(await page.doesInputExist('address')).toBe(true);
    expect(await page.doesInputExist('phoneNumber')).toBe(true);
    expect(await page.doesButtonExist('create')).toBe(true);

    await page.enterCustomerInfo('Araminta', 'Ross', '1849 Harriet Ave, Auburn, NY 23102', '309-555-1370');
    await page.clickButton('create');

    // fields are empty
    expect(await page.getInputValue('firstName')).toBe('');
    expect(await page.getInputValue('lastName')).toBe('');
    expect(await page.getInputValue('address')).toBe('');
    expect(await page.getInputValue('phoneNumber')).toBe('');

  });

  it('customer added to list', async () => {
    await page.navigateTo();

    await page.enterCustomerInfo('Araminta', 'Ross', '1849 Harriet Ave, Auburn, NY 23102', '309-555-1370');
    await page.clickButton('create');

    await page.enterCustomerInfo('John', 'Doe', '123 Main Street, New York NY 12345', '123-456-7890');
    await page.clickButton('create');

    // names added to list
    expect(await page.findInList( 'Araminta', 'Ross')).toBe(true);
    expect(await page.findInList( 'John', 'Doe')).toBe(true);
  });

  it('clicking list shows details', async () => {
    await page.navigateTo();

    await page.enterCustomerInfo('Araminta', 'Ross', '1849 Harriet Ave, Auburn, NY 23102', '309-555-1370');
    await page.clickButton('create');

    await page.enterCustomerInfo('John', 'Doe', '123 Main Street, New York NY 12345', '123-456-7890');
    await page.clickButton('create');

    // click item in list and see details
    await page.clickListItem(0);
    expect(await page.findText('firstNameDetail', 'Araminta')).toBe(true);
    expect(await page.findText('lastNameDetail', 'Ross')).toBe(true);
    expect(await page.findText('addressDetail', '1849 Harriet Ave, Auburn, NY 23102')).toBe(true);
    expect(await page.findText('phoneNumberDetail', '309-555-1370')).toBe(true);
  });

  it('edit details updates customer info', async () => {
    await page.navigateTo();

    await page.enterCustomerInfo('Araminta', 'Ross', '1849 Harriet Ave, Auburn, NY 23102', '309-555-1370');
    await page.clickButton('create');

    // click item in list and see details
    await page.clickListItem(0);

    expect(await page.doesInputExist('firstNameEdit')).toBe(false);
    expect(await page.doesInputExist('lastNameEdit')).toBe(false);
    expect(await page.doesInputExist('addressEdit')).toBe(false);
    expect(await page.doesInputExist('phoneNumberEdit')).toBe(false);

    await page.clickButton('edit');

    expect(await page.doesInputExist('firstNameEdit')).toBe(true);
    expect(await page.doesInputExist('lastNameEdit')).toBe(true);
    expect(await page.doesInputExist('addressEdit')).toBe(true);
    expect(await page.doesInputExist('phoneNumberEdit')).toBe(true);

    await page.editCustomerInfo('John', 'Doe', '123 Main Street, New York NY 12345', '123-456-7890');

    await page.clickButton('update');

    expect(await page.findText('firstNameDetail', 'John')).toBe(true);
    expect(await page.findText('lastNameDetail', 'Doe')).toBe(true);
    expect(await page.findText('addressDetail', '123 Main Street, New York NY 12345')).toBe(true);
    expect(await page.findText('phoneNumberDetail', '123-456-7890')).toBe(true);

  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
