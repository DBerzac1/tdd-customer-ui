import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  firstName = '';
  lastName = '';
  address = '';
  phoneNumber = '';

  firstNameEditValue = '';
  lastNameEditValue = '';
  addressEditValue = '';
  phoneNumberEditValue = '';

  customers: Array<{
    firstName: string,
    lastName: string
    address: string,
    phoneNumber: string }> = [];

  customerIndex = 0;

  editMode = false;

  onClick(): void {

    const newCustomer = {
      firstName: this.firstName,
      lastName: this.lastName,
      address: this.address,
      phoneNumber: this.phoneNumber
    };
    this.customers.push(newCustomer);

    this.firstName = '';
    this.lastName = '';
    this.address = '';
    this.phoneNumber = '';
  }

  onClickCustomer(index: number): void {
    this.customerIndex = index;
  }

  onClickEdit(): void {
    this.firstNameEditValue = this.customers[this.customerIndex].firstName;
    this.lastNameEditValue = this.customers[this.customerIndex].lastName;
    this.addressEditValue = this.customers[this.customerIndex].address;
    this.phoneNumberEditValue = this.customers[this.customerIndex].phoneNumber;
    this.editMode = true;
  }

  onClickUpdate(): void {
    this.customers[this.customerIndex].firstName = this.firstNameEditValue;
    this.customers[this.customerIndex].lastName = this.lastNameEditValue;
    this.customers[this.customerIndex].address = this.addressEditValue;
    this.customers[this.customerIndex].phoneNumber = this.phoneNumberEditValue;
    this.editMode = false;
  }
}
